.. A Byte of Python (Russian) documentation master file, created by
   sphinx-quickstart on Sun Aug 12 18:10:02 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. "Укус Питона" -- "A Byte of Python" по-русски
   =============================================

Содержание
==========

.. **С О Д Е Р Ж А Н И Е**

.. toctree::
   :maxdepth: 2

   frontpage
   translations
   preface
   introduction
   installation
   first_steps
   basics
   operators_and_expressions
   control_flow
   functions
   modules
   data_structures
   problem_solving
   object_oriented_programming
   input_output
   exceptions
   standard_library
   more
   what_next
   appendix_FLOSS
   appendix_about
   appendix_revision_history
   appendix_translation_howto


.. Индексы и таблицы
   =================

   * :ref:`genindex`
   * :ref:`search`

